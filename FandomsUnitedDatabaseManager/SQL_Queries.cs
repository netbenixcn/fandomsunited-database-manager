﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Diagnostics;

namespace FandomsUnitedDatabaseManager {
    class SQL_Queries {

        //Set DataGrid Column Formatting
        public void setDataGridColumnFormatting(Form1 form) {
            //Get the Colums by Name
            DataGridViewColumn colID = form.dgv_out.Columns["ID"];
            DataGridViewColumn colChara = form.dgv_out.Columns["Character"];
            DataGridViewColumn colAnime = form.dgv_out.Columns["Anime"];
            DataGridViewColumn colQuote = form.dgv_out.Columns["Quote"];
            DataGridViewColumn colTimesUsed = form.dgv_out.Columns["TimesUsed"];
            DataGridViewColumn colLastUsed = form.dgv_out.Columns["LastUsed"];
            //Set Column Attributes
            colID.Width = 30;
            colID.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            colID.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            colChara.Width = 140;
            colAnime.Width = 170;
            colQuote.Width = 305;
            colTimesUsed.Width = 70;
            colTimesUsed.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            colTimesUsed.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            colLastUsed.Width = 67;
            colLastUsed.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            colLastUsed.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Disable Sorting
            foreach(DataGridViewColumn column in form.dgv_out.Columns) {
                if (form.combo_ProcedureSelect.SelectedIndex > 1) {
                    column.SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
        }

        //Clear the DataGrid
        public void clearDataGrid(Form1 form) {
            switch (form.combo_ProcedureSelect.SelectedIndex) {
                case 0: { form.lab_var1.Text = "Anime:"; form.enableTxtBoxAndStatus(true); break; }
                case 1: { form.lab_var1.Text = "Character:"; form.enableTxtBoxAndStatus(true); break; }
                default: { form.lab_var1.Hide(); form.enableTxtBoxAndStatus(false); break; }
            }
            form.dgv_out.Columns.Clear();
        }
        
        //Get SQLData for Output
        public void GetDataSQL(Form1 form, MySqlConnection sqlCon) {
            String sqlQuery = "";
            String status;
            if(form.combo_Status.SelectedIndex == 0) {
                status = "USED";
            } else {
                status = "UNUSED";
            }
            String var1 = form.txtBox_var1.Text;
            switch (form.combo_ProcedureSelect.SelectedIndex) {
                case 0: { sqlQuery = "CALL getByAnime('" + var1 + "', '" + status + "');"; break; }
                case 1: { sqlQuery = "CALL getByCharacter('" + var1 + "', '" + status + "');"; break; }
                case 2: { sqlQuery = "CALL getLastUsed();"; break; }
                case 3: { sqlQuery = "CALL getLeastUsed();"; break; }
            }

            //Try to execute Query and catch errors
            try {
                sqlCon.Open();
                MySqlCommand mysqlCom = new MySqlCommand(sqlQuery, sqlCon);
                MySqlDataAdapter sqlDa = new MySqlDataAdapter(mysqlCom);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                form.dgv_out.DataSource = dtbl;
                setDataGridColumnFormatting(form);
                sqlCon.Close();
            } catch(MySqlException sqlEx) {
                MessageBox.Show(sqlEx.Message, "SQL ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Create new Entry in table tblQuotes
        public void CreateNewSQLEntry(Form1 form, MySqlConnection sqlCon) {
            DataTable dtbl = null;
            //Try to get all values
            try {
                sqlCon.Open();
                MySqlDataAdapter sqlDa = new MySqlDataAdapter("SELECT * FROM tblQuotes", sqlCon);
                dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                sqlCon.Close();
            } catch(MySqlException sqlEx) {
                MessageBox.Show(sqlEx.Message, "SQL ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sqlCon.Close();
            }
           

            //Insert SQLQuery
            String sqlQuery = "";
            String quote = form.txtBox_in_Quote.Text;
            int quotePos = 0;
            //Fetch "'" char and put "\" before it for SQL-Compability
            foreach(char c in quote) {
                if (c == Convert.ToChar("'")) {
                    quote = quote.Insert(quotePos, @"\");
                }
                quotePos++;
            }
            //Set the Query
            sqlQuery = "INSERT INTO `tblQuotes` (`ID`, `Character`, `Anime`, `Quote`, `TimesUsed`, `LastUsed`) VALUES " +
                "('" + (dtbl.Rows.Count + 1) + "', '" + form.txtBox_in_chara.Text + "', '" + form.txtBox_in_anime.Text + "', '" + quote + "', '0', '0000-00-00');";
            //Try to execute Query and catch errors
            try {
                sqlCon.Open();
                MySqlCommand mysqlCom = new MySqlCommand(sqlQuery, sqlCon);
                mysqlCom.ExecuteNonQuery();
                sqlCon.Close();
                MessageBox.Show("Erfolgreich!", "SQL Query", MessageBoxButtons.OK);
            } catch(MySqlException sqlEx) {
                MessageBox.Show(sqlEx.Message, "SQL ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sqlCon.Close();
            }
        }

        //Update the Entry in table tblQuotes
        public void CreateUsedQuoteSQLEntry(Form1 form, MySqlConnection sqlCon) {
            DateTime dateRaw = form.dateTime_LastUsed.Value;
            String date = dateRaw.ToString("yyyy-MM-dd");
            //Try to execute Query and catch errors
            try{
                sqlCon.Open();
                MySqlCommand sqlCom = new MySqlCommand("UPDATE tblQuotes SET TimesUsed = '" + (Convert.ToInt32(form.selectedQuoteTimesUsed) + 1) + "', LastUsed = '" + date + "' WHERE ID = " + form.selectedQuoteID + ";", sqlCon);
                sqlCom.ExecuteNonQuery();
                sqlCon.Close();
                MessageBox.Show("Erfolgreich!", "SQL Query", MessageBoxButtons.OK);
            } catch(MySqlException sqlEx) {
                MessageBox.Show(sqlEx.Message, "SQL ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
    }
}
