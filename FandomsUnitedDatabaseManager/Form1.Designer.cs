﻿namespace FandomsUnitedDatabaseManager {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dgv_out = new System.Windows.Forms.DataGridView();
            this.pan_Selection = new System.Windows.Forms.Panel();
            this.lab_status = new System.Windows.Forms.Label();
            this.lab_var1 = new System.Windows.Forms.Label();
            this.combo_ProcedureSelect = new System.Windows.Forms.ComboBox();
            this.combo_Status = new System.Windows.Forms.ComboBox();
            this.txtBox_var1 = new System.Windows.Forms.TextBox();
            this.img_Logo = new System.Windows.Forms.PictureBox();
            this.combo_IO_Selection = new System.Windows.Forms.ComboBox();
            this.pan_input = new System.Windows.Forms.Panel();
            this.pan_usedQuote = new System.Windows.Forms.Panel();
            this.txtBox_selectedQuoteID = new System.Windows.Forms.TextBox();
            this.txtBox_selectedQuoteChara = new System.Windows.Forms.TextBox();
            this.txtBox_selectedQuoteAnime = new System.Windows.Forms.TextBox();
            this.txtBox_selectedQuote = new System.Windows.Forms.TextBox();
            this.lab_selectedQuote = new System.Windows.Forms.Label();
            this.lab_selectedQuoteAnime = new System.Windows.Forms.Label();
            this.lab_selectedQuoteCharacter = new System.Windows.Forms.Label();
            this.lab_selectedQuoteID = new System.Windows.Forms.Label();
            this.btn_UsedQuoteStartSQL = new System.Windows.Forms.Button();
            this.btn_selectQuote = new System.Windows.Forms.Button();
            this.lab_in_usedQuote = new System.Windows.Forms.Label();
            this.dateTime_LastUsed = new System.Windows.Forms.DateTimePicker();
            this.lab_in_dateUsed = new System.Windows.Forms.Label();
            this.pan_newQuote = new System.Windows.Forms.Panel();
            this.btn_NewQuoteStartSQL = new System.Windows.Forms.Button();
            this.lab_in_newAnime = new System.Windows.Forms.Label();
            this.lab_in_newChara = new System.Windows.Forms.Label();
            this.lab_in_newQuote = new System.Windows.Forms.Label();
            this.txtBox_in_anime = new System.Windows.Forms.TextBox();
            this.txtBox_in_Quote = new System.Windows.Forms.TextBox();
            this.txtBox_in_chara = new System.Windows.Forms.TextBox();
            this.but_UsedQuoteMode = new System.Windows.Forms.Button();
            this.but_newQuoteMode = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_out)).BeginInit();
            this.pan_Selection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_Logo)).BeginInit();
            this.pan_input.SuspendLayout();
            this.pan_usedQuote.SuspendLayout();
            this.pan_newQuote.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_out
            // 
            this.dgv_out.AllowUserToAddRows = false;
            this.dgv_out.AllowUserToDeleteRows = false;
            this.dgv_out.AllowUserToResizeColumns = false;
            this.dgv_out.AllowUserToResizeRows = false;
            this.dgv_out.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_out.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_out.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_out.Location = new System.Drawing.Point(346, 32);
            this.dgv_out.Name = "dgv_out";
            this.dgv_out.ReadOnly = true;
            this.dgv_out.RowHeadersWidth = 25;
            this.dgv_out.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_out.Size = new System.Drawing.Size(826, 647);
            this.dgv_out.TabIndex = 0;
            // 
            // pan_Selection
            // 
            this.pan_Selection.BackColor = System.Drawing.Color.SlateGray;
            this.pan_Selection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pan_Selection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pan_Selection.Controls.Add(this.lab_status);
            this.pan_Selection.Controls.Add(this.lab_var1);
            this.pan_Selection.Controls.Add(this.combo_ProcedureSelect);
            this.pan_Selection.Controls.Add(this.combo_Status);
            this.pan_Selection.Controls.Add(this.txtBox_var1);
            this.pan_Selection.Location = new System.Drawing.Point(8, 59);
            this.pan_Selection.Name = "pan_Selection";
            this.pan_Selection.Size = new System.Drawing.Size(332, 281);
            this.pan_Selection.TabIndex = 2;
            // 
            // lab_status
            // 
            this.lab_status.AutoSize = true;
            this.lab_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_status.Location = new System.Drawing.Point(31, 127);
            this.lab_status.Name = "lab_status";
            this.lab_status.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lab_status.Size = new System.Drawing.Size(55, 16);
            this.lab_status.TabIndex = 5;
            this.lab_status.Text = "Status:";
            this.lab_status.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lab_var1
            // 
            this.lab_var1.AutoSize = true;
            this.lab_var1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_var1.Location = new System.Drawing.Point(31, 89);
            this.lab_var1.Name = "lab_var1";
            this.lab_var1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lab_var1.Size = new System.Drawing.Size(55, 16);
            this.lab_var1.TabIndex = 4;
            this.lab_var1.Text = "Anime:";
            this.lab_var1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // combo_ProcedureSelect
            // 
            this.combo_ProcedureSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combo_ProcedureSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_ProcedureSelect.FormattingEnabled = true;
            this.combo_ProcedureSelect.Items.AddRange(new object[] {
            "ByAnime",
            "ByCharacter",
            "Zuletzt Benutzt",
            "Am wenigsten Benutzt"});
            this.combo_ProcedureSelect.Location = new System.Drawing.Point(34, 28);
            this.combo_ProcedureSelect.Name = "combo_ProcedureSelect";
            this.combo_ProcedureSelect.Size = new System.Drawing.Size(263, 21);
            this.combo_ProcedureSelect.TabIndex = 3;
            this.combo_ProcedureSelect.SelectedIndexChanged += new System.EventHandler(this.refreshDataGrid);
            // 
            // combo_Status
            // 
            this.combo_Status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_Status.FormattingEnabled = true;
            this.combo_Status.Items.AddRange(new object[] {
            "Benutzt",
            "Unbenutzt"});
            this.combo_Status.Location = new System.Drawing.Point(141, 126);
            this.combo_Status.Name = "combo_Status";
            this.combo_Status.Size = new System.Drawing.Size(156, 21);
            this.combo_Status.TabIndex = 1;
            this.combo_Status.SelectedIndexChanged += new System.EventHandler(this.refreshDataGrid);
            // 
            // txtBox_var1
            // 
            this.txtBox_var1.Location = new System.Drawing.Point(141, 86);
            this.txtBox_var1.Name = "txtBox_var1";
            this.txtBox_var1.Size = new System.Drawing.Size(156, 20);
            this.txtBox_var1.TabIndex = 0;
            this.txtBox_var1.TextChanged += new System.EventHandler(this.refreshDataGrid);
            // 
            // img_Logo
            // 
            this.img_Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.img_Logo.Image = ((System.Drawing.Image)(resources.GetObject("img_Logo.Image")));
            this.img_Logo.Location = new System.Drawing.Point(10, 420);
            this.img_Logo.Name = "img_Logo";
            this.img_Logo.Size = new System.Drawing.Size(320, 279);
            this.img_Logo.TabIndex = 3;
            this.img_Logo.TabStop = false;
            // 
            // combo_IO_Selection
            // 
            this.combo_IO_Selection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_IO_Selection.FormattingEnabled = true;
            this.combo_IO_Selection.Items.AddRange(new object[] {
            "Ausgabe",
            "Eingabe"});
            this.combo_IO_Selection.Location = new System.Drawing.Point(12, 32);
            this.combo_IO_Selection.Name = "combo_IO_Selection";
            this.combo_IO_Selection.Size = new System.Drawing.Size(328, 21);
            this.combo_IO_Selection.TabIndex = 4;
            this.combo_IO_Selection.SelectedIndexChanged += new System.EventHandler(this.inputOutputSwitch);
            // 
            // pan_input
            // 
            this.pan_input.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pan_input.Controls.Add(this.pan_usedQuote);
            this.pan_input.Controls.Add(this.pan_newQuote);
            this.pan_input.Controls.Add(this.but_UsedQuoteMode);
            this.pan_input.Controls.Add(this.but_newQuoteMode);
            this.pan_input.Location = new System.Drawing.Point(346, 32);
            this.pan_input.Name = "pan_input";
            this.pan_input.Size = new System.Drawing.Size(826, 647);
            this.pan_input.TabIndex = 5;
            // 
            // pan_usedQuote
            // 
            this.pan_usedQuote.BackColor = System.Drawing.Color.SlateGray;
            this.pan_usedQuote.Controls.Add(this.txtBox_selectedQuoteID);
            this.pan_usedQuote.Controls.Add(this.txtBox_selectedQuoteChara);
            this.pan_usedQuote.Controls.Add(this.txtBox_selectedQuoteAnime);
            this.pan_usedQuote.Controls.Add(this.txtBox_selectedQuote);
            this.pan_usedQuote.Controls.Add(this.lab_selectedQuote);
            this.pan_usedQuote.Controls.Add(this.lab_selectedQuoteAnime);
            this.pan_usedQuote.Controls.Add(this.lab_selectedQuoteCharacter);
            this.pan_usedQuote.Controls.Add(this.lab_selectedQuoteID);
            this.pan_usedQuote.Controls.Add(this.btn_UsedQuoteStartSQL);
            this.pan_usedQuote.Controls.Add(this.btn_selectQuote);
            this.pan_usedQuote.Controls.Add(this.lab_in_usedQuote);
            this.pan_usedQuote.Controls.Add(this.dateTime_LastUsed);
            this.pan_usedQuote.Controls.Add(this.lab_in_dateUsed);
            this.pan_usedQuote.Location = new System.Drawing.Point(108, 364);
            this.pan_usedQuote.Name = "pan_usedQuote";
            this.pan_usedQuote.Size = new System.Drawing.Size(605, 126);
            this.pan_usedQuote.TabIndex = 9;
            // 
            // txtBox_selectedQuoteID
            // 
            this.txtBox_selectedQuoteID.Location = new System.Drawing.Point(354, 9);
            this.txtBox_selectedQuoteID.Name = "txtBox_selectedQuoteID";
            this.txtBox_selectedQuoteID.ReadOnly = true;
            this.txtBox_selectedQuoteID.Size = new System.Drawing.Size(238, 20);
            this.txtBox_selectedQuoteID.TabIndex = 15;
            // 
            // txtBox_selectedQuoteChara
            // 
            this.txtBox_selectedQuoteChara.Location = new System.Drawing.Point(403, 28);
            this.txtBox_selectedQuoteChara.Name = "txtBox_selectedQuoteChara";
            this.txtBox_selectedQuoteChara.ReadOnly = true;
            this.txtBox_selectedQuoteChara.Size = new System.Drawing.Size(189, 20);
            this.txtBox_selectedQuoteChara.TabIndex = 14;
            // 
            // txtBox_selectedQuoteAnime
            // 
            this.txtBox_selectedQuoteAnime.Location = new System.Drawing.Point(380, 47);
            this.txtBox_selectedQuoteAnime.Name = "txtBox_selectedQuoteAnime";
            this.txtBox_selectedQuoteAnime.ReadOnly = true;
            this.txtBox_selectedQuoteAnime.Size = new System.Drawing.Size(212, 20);
            this.txtBox_selectedQuoteAnime.TabIndex = 13;
            // 
            // txtBox_selectedQuote
            // 
            this.txtBox_selectedQuote.Location = new System.Drawing.Point(380, 67);
            this.txtBox_selectedQuote.Multiline = true;
            this.txtBox_selectedQuote.Name = "txtBox_selectedQuote";
            this.txtBox_selectedQuote.ReadOnly = true;
            this.txtBox_selectedQuote.Size = new System.Drawing.Size(212, 45);
            this.txtBox_selectedQuote.TabIndex = 12;
            // 
            // lab_selectedQuote
            // 
            this.lab_selectedQuote.AutoSize = true;
            this.lab_selectedQuote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_selectedQuote.Location = new System.Drawing.Point(331, 64);
            this.lab_selectedQuote.Name = "lab_selectedQuote";
            this.lab_selectedQuote.Size = new System.Drawing.Size(53, 16);
            this.lab_selectedQuote.TabIndex = 11;
            this.lab_selectedQuote.Text = "Quote:";
            // 
            // lab_selectedQuoteAnime
            // 
            this.lab_selectedQuoteAnime.AutoSize = true;
            this.lab_selectedQuoteAnime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_selectedQuoteAnime.Location = new System.Drawing.Point(332, 48);
            this.lab_selectedQuoteAnime.Name = "lab_selectedQuoteAnime";
            this.lab_selectedQuoteAnime.Size = new System.Drawing.Size(55, 16);
            this.lab_selectedQuoteAnime.TabIndex = 10;
            this.lab_selectedQuoteAnime.Text = "Anime:";
            // 
            // lab_selectedQuoteCharacter
            // 
            this.lab_selectedQuoteCharacter.AutoSize = true;
            this.lab_selectedQuoteCharacter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_selectedQuoteCharacter.Location = new System.Drawing.Point(323, 30);
            this.lab_selectedQuoteCharacter.Name = "lab_selectedQuoteCharacter";
            this.lab_selectedQuoteCharacter.Size = new System.Drawing.Size(75, 16);
            this.lab_selectedQuoteCharacter.TabIndex = 9;
            this.lab_selectedQuoteCharacter.Text = "Charakter";
            // 
            // lab_selectedQuoteID
            // 
            this.lab_selectedQuoteID.AutoSize = true;
            this.lab_selectedQuoteID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_selectedQuoteID.Location = new System.Drawing.Point(327, 12);
            this.lab_selectedQuoteID.Name = "lab_selectedQuoteID";
            this.lab_selectedQuoteID.Size = new System.Drawing.Size(27, 16);
            this.lab_selectedQuoteID.TabIndex = 8;
            this.lab_selectedQuoteID.Text = "ID:";
            // 
            // btn_UsedQuoteStartSQL
            // 
            this.btn_UsedQuoteStartSQL.Location = new System.Drawing.Point(153, 89);
            this.btn_UsedQuoteStartSQL.Name = "btn_UsedQuoteStartSQL";
            this.btn_UsedQuoteStartSQL.Size = new System.Drawing.Size(75, 23);
            this.btn_UsedQuoteStartSQL.TabIndex = 7;
            this.btn_UsedQuoteStartSQL.Text = "Speichern";
            this.btn_UsedQuoteStartSQL.UseVisualStyleBackColor = true;
            this.btn_UsedQuoteStartSQL.Click += new System.EventHandler(this.btn_UsedQuoteStartSQL_Click);
            // 
            // btn_selectQuote
            // 
            this.btn_selectQuote.Location = new System.Drawing.Point(133, 19);
            this.btn_selectQuote.Name = "btn_selectQuote";
            this.btn_selectQuote.Size = new System.Drawing.Size(91, 23);
            this.btn_selectQuote.TabIndex = 3;
            this.btn_selectQuote.Text = "Select Quote";
            this.btn_selectQuote.UseVisualStyleBackColor = true;
            this.btn_selectQuote.Click += new System.EventHandler(this.btn_selectQuote_Click);
            // 
            // lab_in_usedQuote
            // 
            this.lab_in_usedQuote.AutoSize = true;
            this.lab_in_usedQuote.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_in_usedQuote.Location = new System.Drawing.Point(56, 19);
            this.lab_in_usedQuote.Name = "lab_in_usedQuote";
            this.lab_in_usedQuote.Size = new System.Drawing.Size(73, 24);
            this.lab_in_usedQuote.TabIndex = 2;
            this.lab_in_usedQuote.Text = "Quote:";
            // 
            // dateTime_LastUsed
            // 
            this.dateTime_LastUsed.Location = new System.Drawing.Point(133, 55);
            this.dateTime_LastUsed.Name = "dateTime_LastUsed";
            this.dateTime_LastUsed.Size = new System.Drawing.Size(193, 20);
            this.dateTime_LastUsed.TabIndex = 1;
            // 
            // lab_in_dateUsed
            // 
            this.lab_in_dateUsed.AutoSize = true;
            this.lab_in_dateUsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_in_dateUsed.Location = new System.Drawing.Point(52, 53);
            this.lab_in_dateUsed.Name = "lab_in_dateUsed";
            this.lab_in_dateUsed.Size = new System.Drawing.Size(75, 24);
            this.lab_in_dateUsed.TabIndex = 0;
            this.lab_in_dateUsed.Text = "Datum:";
            // 
            // pan_newQuote
            // 
            this.pan_newQuote.BackColor = System.Drawing.Color.SlateGray;
            this.pan_newQuote.Controls.Add(this.btn_NewQuoteStartSQL);
            this.pan_newQuote.Controls.Add(this.lab_in_newAnime);
            this.pan_newQuote.Controls.Add(this.lab_in_newChara);
            this.pan_newQuote.Controls.Add(this.lab_in_newQuote);
            this.pan_newQuote.Controls.Add(this.txtBox_in_anime);
            this.pan_newQuote.Controls.Add(this.txtBox_in_Quote);
            this.pan_newQuote.Controls.Add(this.txtBox_in_chara);
            this.pan_newQuote.Location = new System.Drawing.Point(108, 83);
            this.pan_newQuote.Name = "pan_newQuote";
            this.pan_newQuote.Size = new System.Drawing.Size(605, 275);
            this.pan_newQuote.TabIndex = 8;
            // 
            // btn_NewQuoteStartSQL
            // 
            this.btn_NewQuoteStartSQL.Location = new System.Drawing.Point(268, 239);
            this.btn_NewQuoteStartSQL.Name = "btn_NewQuoteStartSQL";
            this.btn_NewQuoteStartSQL.Size = new System.Drawing.Size(75, 23);
            this.btn_NewQuoteStartSQL.TabIndex = 6;
            this.btn_NewQuoteStartSQL.Text = "Speichern";
            this.btn_NewQuoteStartSQL.UseVisualStyleBackColor = true;
            this.btn_NewQuoteStartSQL.Click += new System.EventHandler(this.btn_NewQuoteStartSQL_Click);
            // 
            // lab_in_newAnime
            // 
            this.lab_in_newAnime.AutoSize = true;
            this.lab_in_newAnime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_in_newAnime.Location = new System.Drawing.Point(43, 63);
            this.lab_in_newAnime.Name = "lab_in_newAnime";
            this.lab_in_newAnime.Size = new System.Drawing.Size(84, 25);
            this.lab_in_newAnime.TabIndex = 0;
            this.lab_in_newAnime.Text = "Anime:";
            // 
            // lab_in_newChara
            // 
            this.lab_in_newChara.AutoSize = true;
            this.lab_in_newChara.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_in_newChara.Location = new System.Drawing.Point(8, 19);
            this.lab_in_newChara.Name = "lab_in_newChara";
            this.lab_in_newChara.Size = new System.Drawing.Size(122, 25);
            this.lab_in_newChara.TabIndex = 1;
            this.lab_in_newChara.Text = "Charakter:";
            // 
            // lab_in_newQuote
            // 
            this.lab_in_newQuote.AutoSize = true;
            this.lab_in_newQuote.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_in_newQuote.Location = new System.Drawing.Point(45, 115);
            this.lab_in_newQuote.Name = "lab_in_newQuote";
            this.lab_in_newQuote.Size = new System.Drawing.Size(82, 25);
            this.lab_in_newQuote.TabIndex = 2;
            this.lab_in_newQuote.Text = "Quote:";
            // 
            // txtBox_in_anime
            // 
            this.txtBox_in_anime.Location = new System.Drawing.Point(135, 66);
            this.txtBox_in_anime.Name = "txtBox_in_anime";
            this.txtBox_in_anime.Size = new System.Drawing.Size(456, 20);
            this.txtBox_in_anime.TabIndex = 5;
            this.txtBox_in_anime.TextChanged += new System.EventHandler(this.NewQuoteStartUnlocker);
            // 
            // txtBox_in_Quote
            // 
            this.txtBox_in_Quote.Location = new System.Drawing.Point(133, 119);
            this.txtBox_in_Quote.MaxLength = 600;
            this.txtBox_in_Quote.Multiline = true;
            this.txtBox_in_Quote.Name = "txtBox_in_Quote";
            this.txtBox_in_Quote.Size = new System.Drawing.Size(459, 105);
            this.txtBox_in_Quote.TabIndex = 3;
            this.txtBox_in_Quote.TextChanged += new System.EventHandler(this.NewQuoteStartUnlocker);
            // 
            // txtBox_in_chara
            // 
            this.txtBox_in_chara.Location = new System.Drawing.Point(136, 24);
            this.txtBox_in_chara.Name = "txtBox_in_chara";
            this.txtBox_in_chara.Size = new System.Drawing.Size(456, 20);
            this.txtBox_in_chara.TabIndex = 4;
            this.txtBox_in_chara.TextChanged += new System.EventHandler(this.NewQuoteStartUnlocker);
            // 
            // but_UsedQuoteMode
            // 
            this.but_UsedQuoteMode.Location = new System.Drawing.Point(406, 15);
            this.but_UsedQuoteMode.Name = "but_UsedQuoteMode";
            this.but_UsedQuoteMode.Size = new System.Drawing.Size(117, 62);
            this.but_UsedQuoteMode.TabIndex = 7;
            this.but_UsedQuoteMode.Text = "Benutze Quote";
            this.but_UsedQuoteMode.UseVisualStyleBackColor = true;
            this.but_UsedQuoteMode.Click += new System.EventHandler(this.openUsedQuotePanel);
            // 
            // but_newQuoteMode
            // 
            this.but_newQuoteMode.Location = new System.Drawing.Point(285, 15);
            this.but_newQuoteMode.Name = "but_newQuoteMode";
            this.but_newQuoteMode.Size = new System.Drawing.Size(117, 62);
            this.but_newQuoteMode.TabIndex = 6;
            this.but_newQuoteMode.Text = "Neue Quote";
            this.but_newQuoteMode.UseVisualStyleBackColor = true;
            this.but_newQuoteMode.Click += new System.EventHandler(this.openNewQuotePanel);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1184, 691);
            this.Controls.Add(this.pan_input);
            this.Controls.Add(this.combo_IO_Selection);
            this.Controls.Add(this.img_Logo);
            this.Controls.Add(this.pan_Selection);
            this.Controls.Add(this.dgv_out);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "FandomsUnited Database Manager";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_out)).EndInit();
            this.pan_Selection.ResumeLayout(false);
            this.pan_Selection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img_Logo)).EndInit();
            this.pan_input.ResumeLayout(false);
            this.pan_usedQuote.ResumeLayout(false);
            this.pan_usedQuote.PerformLayout();
            this.pan_newQuote.ResumeLayout(false);
            this.pan_newQuote.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pan_Selection;
        private System.Windows.Forms.PictureBox img_Logo;
        private System.Windows.Forms.ComboBox combo_IO_Selection;
        private System.Windows.Forms.Panel pan_input;
        private System.Windows.Forms.Label lab_in_newQuote;
        private System.Windows.Forms.Label lab_in_newChara;
        private System.Windows.Forms.Label lab_in_newAnime;
        private System.Windows.Forms.Panel pan_usedQuote;
        private System.Windows.Forms.Label lab_in_dateUsed;
        private System.Windows.Forms.Panel pan_newQuote;
        private System.Windows.Forms.Button but_UsedQuoteMode;
        private System.Windows.Forms.Button but_newQuoteMode;
        private System.Windows.Forms.Label lab_in_usedQuote;
        private System.Windows.Forms.Button btn_selectQuote;
        private System.Windows.Forms.Button btn_NewQuoteStartSQL;
        public System.Windows.Forms.Button btn_UsedQuoteStartSQL;
        public System.Windows.Forms.DataGridView dgv_out;
        public System.Windows.Forms.Label lab_status;
        public System.Windows.Forms.Label lab_var1;
        public System.Windows.Forms.ComboBox combo_ProcedureSelect;
        public System.Windows.Forms.ComboBox combo_Status;
        public System.Windows.Forms.TextBox txtBox_var1;
        public System.Windows.Forms.TextBox txtBox_in_anime;
        public System.Windows.Forms.TextBox txtBox_in_chara;
        public System.Windows.Forms.TextBox txtBox_in_Quote;
        private System.Windows.Forms.TextBox txtBox_selectedQuoteID;
        private System.Windows.Forms.TextBox txtBox_selectedQuoteChara;
        private System.Windows.Forms.TextBox txtBox_selectedQuoteAnime;
        private System.Windows.Forms.TextBox txtBox_selectedQuote;
        private System.Windows.Forms.Label lab_selectedQuote;
        private System.Windows.Forms.Label lab_selectedQuoteAnime;
        private System.Windows.Forms.Label lab_selectedQuoteCharacter;
        private System.Windows.Forms.Label lab_selectedQuoteID;
        public System.Windows.Forms.DateTimePicker dateTime_LastUsed;
    }
}

