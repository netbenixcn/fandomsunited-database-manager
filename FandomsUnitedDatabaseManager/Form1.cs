﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace FandomsUnitedDatabaseManager {
    public partial class Form1 : Form {

        //Class for SQL Queries
        SQL_Queries sqlClass = new SQL_Queries();

        /// The connection string is saved in another class for security reasons.
        /// The Line looks like this:  public MySqlConnection sqlCon = new MySqlConnection(CON_STRING_HERE);
        SQLConnectionString sqlConStringClass = new SQLConnectionString(); //Class from connectionString
        MySqlConnection sqlCon; //Local connectionString Var

        //Objects for the selected Quote
        public object selectedQuoteID;
        public object selectedQuoteCharacter;
        public object selectedQuoteAnime;
        public object selectedQuote;
        public object selectedQuoteTimesUsed;
          
        public Form1() {
            InitializeComponent();
            sqlCon = sqlConStringClass.sqlCon; //Get and Set Connection String from another Class
            this.MaximumSize = new Size(1200, 730); //Set the Maximum Size of the Window
            this.MinimumSize = new Size(1200, 730); //Set the Minimum Site of the Window
            setDefaults(); //Set the default values
        }



        /////////////////
        /// FUNCTIONS ///
        /////////////////

        //Set the default values
        private void setDefaults() {
            combo_ProcedureSelect.SelectedIndex = 0;
            combo_Status.SelectedIndex = 0;
            combo_IO_Selection.SelectedIndex = 0;
            pan_usedQuote.Hide();
            pan_newQuote.Hide();
            btn_UsedQuoteStartSQL.Enabled = false;
            btn_NewQuoteStartSQL.Enabled = false;
            dateTime_LastUsed.Value = DateTime.Now;
        }

        //Clears the Values of ALL input fields
        private void clearNewQuoteValues() {
            txtBox_in_anime.Text = "";
            txtBox_in_chara.Text = "";
            txtBox_in_Quote.Text = "";
            txtBox_var1.Text = "";
        }

        //Clears the Values of the UsedQuote section
        private void clearUsedQuoteValues() {
            txtBox_selectedQuoteID.Text = "";
            txtBox_selectedQuoteAnime.Text = "";
            txtBox_selectedQuoteChara.Text = "";
            txtBox_selectedQuote.Text = "";
            selectedQuoteID = null;
            selectedQuoteAnime = null;
            selectedQuoteCharacter = null;
            selectedQuote = null;
            selectedQuoteTimesUsed = null;
            btn_UsedQuoteStartSQL.Enabled = false;
        }

        //Fill SelectedQuote Segment
        public void fillSelectedQuote() {
            txtBox_selectedQuoteID.Text = selectedQuoteID.ToString();
            txtBox_selectedQuoteAnime.Text = selectedQuoteAnime.ToString();
            txtBox_selectedQuoteChara.Text = selectedQuoteCharacter.ToString();
            txtBox_selectedQuote.Text = selectedQuote.ToString();
            btn_UsedQuoteStartSQL.Enabled = true;
        }


        //Enable Text and InputBoxes for GetByAnime and GetByCharacter
        public void enableTxtBoxAndStatus(bool state) {
            if (state) {
                lab_var1.Show();
                txtBox_var1.Show();
                lab_status.Show();
                combo_Status.Show();
            } else {
                lab_var1.Hide();
                txtBox_var1.Hide();
                lab_status.Hide();
                combo_Status.Hide();
            }
        }



        /////////////////////
        /// BUTTON EVENTS ///
        /////////////////////
        
        //Change between Input and Output panels
        private void inputOutputSwitch(object sender, EventArgs e) {
            if (combo_IO_Selection.SelectedIndex == 0) {
                pan_Selection.Show();
                dgv_out.Show();
                pan_input.Hide();
                pan_newQuote.Hide();
                pan_usedQuote.Hide();
            } else {
                pan_Selection.Hide();
                dgv_out.Hide();
                pan_input.Show();
            }
        }

        //Open Panel for New Quotes
        private void openNewQuotePanel(object sender, EventArgs e) {
            pan_newQuote.Show();
            pan_usedQuote.Hide();
        }

        //Open Panel for Used Quotes
        private void openUsedQuotePanel(object sender, EventArgs e) {
            pan_usedQuote.Show();
            pan_newQuote.Hide();
        }


        //Clear the DataGrid
        private void clearDataGrid(object sender, EventArgs e) {
           
        }

        //Start SQL Insert to tblQuotes
        private void btn_NewQuoteStartSQL_Click(object sender, EventArgs e) {
            sqlClass.CreateNewSQLEntry(this, sqlCon);
            clearNewQuoteValues();
        }

        //Start SQL Upadte to tblQuotes
        private void btn_UsedQuoteStartSQL_Click(object sender, EventArgs e) {
            sqlClass.CreateUsedQuoteSQLEntry(this, sqlCon);
            clearUsedQuoteValues();
        }

        //Select Quote for UsedQuotes
        private void btn_selectQuote_Click(object sender, EventArgs e) {
            SelectQuoteForm quoteForm = new SelectQuoteForm(this, sqlCon);
            quoteForm.Show();
        }

        //Locks or Unlocks the SQLStart Button for the NewQuote Section
        private void NewQuoteStartUnlocker(object sender, EventArgs e) {
            if(txtBox_in_anime.Text == "" || txtBox_in_chara.Text == "" || txtBox_in_Quote.Text == "") {
                btn_NewQuoteStartSQL.Enabled = false;
            } else {
                btn_NewQuoteStartSQL.Enabled = true;
            }
        } 

        //Refresh DataGrid on Text change
        private void refreshDataGrid(object sender, EventArgs e) {
            sqlClass.clearDataGrid(this);
            if (combo_ProcedureSelect.SelectedIndex > 1) {
                sqlClass.GetDataSQL(this, sqlCon);
            }
            sqlClass.GetDataSQL(this, sqlCon);
        }

    }
}
