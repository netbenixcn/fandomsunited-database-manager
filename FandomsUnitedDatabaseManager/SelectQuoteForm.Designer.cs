﻿namespace FandomsUnitedDatabaseManager {
    partial class SelectQuoteForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectQuoteForm));
            this.btn_EnterSelection = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.combo_searchBy = new System.Windows.Forms.ComboBox();
            this.txtBox_search = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_EnterSelection
            // 
            this.btn_EnterSelection.Location = new System.Drawing.Point(368, 445);
            this.btn_EnterSelection.Name = "btn_EnterSelection";
            this.btn_EnterSelection.Size = new System.Drawing.Size(75, 23);
            this.btn_EnterSelection.TabIndex = 0;
            this.btn_EnterSelection.Text = "Auswählen";
            this.btn_EnterSelection.UseVisualStyleBackColor = true;
            this.btn_EnterSelection.Click += new System.EventHandler(this.getSelection);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 42);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 25;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(811, 397);
            this.dataGridView1.TabIndex = 1;
            // 
            // combo_searchBy
            // 
            this.combo_searchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_searchBy.FormattingEnabled = true;
            this.combo_searchBy.Items.AddRange(new object[] {
            "ID",
            "Charakter",
            "Anime",
            "Quote"});
            this.combo_searchBy.Location = new System.Drawing.Point(12, 12);
            this.combo_searchBy.Name = "combo_searchBy";
            this.combo_searchBy.Size = new System.Drawing.Size(121, 21);
            this.combo_searchBy.TabIndex = 2;
            this.combo_searchBy.SelectedIndexChanged += new System.EventHandler(this.changedComboBox);
            // 
            // txtBox_search
            // 
            this.txtBox_search.Location = new System.Drawing.Point(139, 13);
            this.txtBox_search.Name = "txtBox_search";
            this.txtBox_search.Size = new System.Drawing.Size(249, 20);
            this.txtBox_search.TabIndex = 3;
            this.txtBox_search.TextChanged += new System.EventHandler(this.refreshDataGrid);
            // 
            // SelectQuoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(834, 481);
            this.Controls.Add(this.txtBox_search);
            this.Controls.Add(this.combo_searchBy);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_EnterSelection);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectQuoteForm";
            this.Text = "Select Quote";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_EnterSelection;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox combo_searchBy;
        private System.Windows.Forms.TextBox txtBox_search;
    }
}