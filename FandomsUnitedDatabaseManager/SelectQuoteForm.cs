﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace FandomsUnitedDatabaseManager {
    public partial class SelectQuoteForm : Form {

        Form1 masterForm;
        MySqlConnection sqlCon;
        MySqlCommand mysqlCom;
        public SelectQuoteForm(Form1 form, MySqlConnection sqlCon) {
            InitializeComponent();
            this.sqlCon = sqlCon;
            masterForm = form;
            this.MaximumSize = new Size(850, 520); //Set the Maximum Size of the Window
            this.MinimumSize = new Size(850, 520); //Set the Minimum Size of the Window
            combo_searchBy.SelectedIndex = 0;

            startSQL(); //Get All Values from Table
            setDataGridFormatting(); //Format the DataGrid
        }



        /////////////////
        /// FUNCTIONS ///
        /////////////////

        //Format the DataGrid
        private void setDataGridFormatting() {
            dataGridView1.Columns["ID"].Width = 30;
            dataGridView1.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["ID"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["Anime"].Width = 170;
            dataGridView1.Columns["Character"].Width = 140;
            dataGridView1.Columns["Quote"].Width = 287;
            dataGridView1.Columns["TimesUsed"].Width = 70;
            dataGridView1.Columns["LastUsed"].Width = 70;
            dataGridView1.Columns["TimesUsed"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["TimesUsed"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["LastUsed"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["LastUsed"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        //Get All Values from Table
        private void startSQL() {
                switch (combo_searchBy.SelectedIndex) {
                    case 0: {
                            if(txtBox_search.Text == "") {
                                mysqlCom = new MySqlCommand("SELECT * FROM tblQuotes;", sqlCon);
                            } else {
                                mysqlCom = new MySqlCommand("SELECT * FROM tblQuotes WHERE tblQuotes.ID = " + txtBox_search.Text + ";", sqlCon);
                            }
                            break;
                        }
                    case 1: {
                            if (txtBox_search.Text == "") {
                                mysqlCom = new MySqlCommand("SELECT * FROM tblQuotes;", sqlCon);
                            } else {
                                mysqlCom = new MySqlCommand("SELECT * FROM tblQuotes WHERE tblQuotes.Character LIKE CONCAT('%', '" + txtBox_search.Text + "', '%');", sqlCon);
                            }
                            break;
                        }
                    case 2: {
                            if (txtBox_search.Text == "") {
                                mysqlCom = new MySqlCommand("SELECT * FROM tblQuotes;", sqlCon);
                            } else {
                                mysqlCom = new MySqlCommand("SELECT * FROM tblQuotes WHERE tblQuotes.Anime LIKE CONCAT('%', '" + txtBox_search.Text + "', '%');", sqlCon);
                            }
                            break;
                        }
                    case 3: {
                            if (txtBox_search.Text == "") {
                                mysqlCom = new MySqlCommand("SELECT * FROM tblQuotes;", sqlCon);
                            } else {
                                String quote = txtBox_search.Text;
                                int quotePos = 0;
                            //Fetch "'" char and put "\" before it for SQL-Compability
                            foreach (char c in quote) {
                                    if (c == Convert.ToChar("'")) {
                                       quote = quote.Insert(quotePos, @"\");
                                    }
                                     quotePos++;
                                }
                            mysqlCom = new MySqlCommand("SELECT * FROM tblQuotes WHERE tblQuotes.Quote LIKE CONCAT('%', '" + quote + "', '%');", sqlCon);
                            }
                            break;
                        }
                }


            //Try to execute Query and catch errors
            try {
                sqlCon.Open();
                MySqlDataAdapter sqlDa = new MySqlDataAdapter(mysqlCom);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                dataGridView1.DataSource = dtbl;
                sqlCon.Close();
                mysqlCom = null;
            } catch (MySqlException sqlEx) {
                MessageBox.Show(sqlEx.Message, "SQL ERROR", MessageBoxButtons.OK);
                sqlCon.Close();
            }
        }
        


        /////////////////////
        /// BUTTON EVENTS ///
        /////////////////////

        //Reset txtBox if SelectedIndex of comboBox changed
        private void changedComboBox(object sender, EventArgs e) {
            txtBox_search.Text = "";
        }

        //Refresh DataGrid after each change in txtBox
        private void refreshDataGrid(object sender, EventArgs e) {
            if(txtBox_search.Text != "" && combo_searchBy.SelectedIndex == 0) {
                byte[] asciiBytes = Encoding.ASCII.GetBytes(txtBox_search.Text);
                if (asciiBytes[0] < 48 || asciiBytes[0] > 57) {
                    txtBox_search.Text = "";
                }
            } 
            //Get Values
            startSQL();
        }

        //Get Values of selected Quote
        private void getSelection(object sender, EventArgs e) {
            masterForm.selectedQuoteID = dataGridView1.SelectedRows[0].Cells["ID"].Value;
            masterForm.selectedQuoteAnime = dataGridView1.SelectedRows[0].Cells["Anime"].Value;
            masterForm.selectedQuoteCharacter = dataGridView1.SelectedRows[0].Cells["Character"].Value;
            masterForm.selectedQuote = dataGridView1.SelectedRows[0].Cells["Quote"].Value;
            masterForm.selectedQuoteTimesUsed = dataGridView1.SelectedRows[0].Cells["TimesUsed"].Value;
            masterForm.fillSelectedQuote();
            this.Close();
        }
    }
}
