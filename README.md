# FandomsUnited Database Manager #

Just a simple C# Database Manager which was designed for private use.

### What is this repository for? ###

*A Database Manager which is based on the needs for my little Project that is going on*

### How do I get set up? ###

Step #1 Download the Repo, Create a SQLTable(Name MUST be "tblQuotes") with the following columns:

* ID | INT(11)

* Character | VARCHAR(45)

* Anime | VARCHAR(60)

* Quote | TEXT(600)

* TimesUsed | INT(11)

* LastUsed | DATE


Step #2 Edit the connectionString in Form1.cs Line #22 like the Comment states and remove Line #21

Step #3 Create the 4 Procedures from the File SQLproceduresCode.txt

Step #4 Fill the Table with some Sample values.
